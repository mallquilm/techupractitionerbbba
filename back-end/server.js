var express = require('express');
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var fs = require('fs');
var requestJSON = require('request-json');


var app = express();
var totalUsers = 0;
app.use(bodyParser.json());

const URL_BASE = '/apitechu/v1/';
const PORT = process.env.PORT || 3000;
//const baseMLabURL = 'https://api.mlab.com/api/1/databases/techu25db/collections/';
const baseMLabURL = 'https://api.mlab.com/api/1/databases/techu25db/collections/';
const apiKey = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';

//Peticion PUT de usuario (by id)
app.put(URL_BASE + 'users/:id',
function(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"id":' + id + '}&';
 var clienteMlab = requestJSON.createClient(baseMLabURL);
 clienteMlab.get('user?'+ queryStringID + apiKey,
   function(error, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
    clienteMlab.put(baseMLabURL +'user?' + queryStringID + apiKey, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
       console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
      res.send(body);
     });
   });
});

//DELETE user with id
app.delete(URL_BASE + "users/:id",
 function(req, res){
   var id = req.params.id;
   var queryStringID = 'q={"id":' + id + '}&';
   console.log(baseMLabURL + 'user?' + queryStringID + apiKey);
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('user?' +  queryStringID + apiKey,
     function(error, respuestaMLab, body){
       var respuesta = body[0];
       httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apiKey,
         function(error, respuestaMLab,body){
           res.send(body);
       });
     });
 });

//Peticion GET de todos los usuarios (collections) OLD
/*
app.get(URL_BASE +'users',function(req,res){
    console.log('GET'+URL_BASE+'users');
    res.send(userFile);
});
*/


//Peticion GET de todos los usuarios (collections) con query string
app.get(URL_BASE +'userlist',function(req,res){
    console.log('GET'+URL_BASE+'userlist');
    var userlist =[];
    var mensaje = null;
    if(parseInt(req.query.inf)<=parseInt(req.query.sup)){
      for(var i = 0; i < userFile.length; i++) {
      var obj = userFile[i];
        if(i>=req.query.inf-1 && i<=req.query.sup-1)
            userlist.push(obj);
      }
    }else {
      mensaje = "El parametro inf debe ser menor o igual que el parametro sup."
    }
    console.log(userlist.length);
    res.send(userlist.length>0?userlist:mensaje);
});

//Peticion GET de un usuario (Instance) OLD
/*
app.get(URL_BASE+'users/:id',function(req,res){
    console.log('GET'+URL_BASE+'users');
    console.log(req.params.id);
    let indice = req.params.id;
    let instancia = userFile[indice-1];
    console.log(instancia);
    let respuesta = (instancia != undefined) ? instancia :{"mensaje":"Recurso no encontrado"};
    res.status(222);
    res.send(respuesta);
})
*/


// GET users consumiendo API REST de mLab
app.get(URL_BASE + 'users',
 function(req, res) {
   console.log("GET /apitechu/v1/user");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('user?' + queryString + apiKey,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});




//Peticion POST con query String
/*
app.get(URL_BASE+'usersq',function(req,res){

    res.send(respuesta);
})
*/

//Nuevo usuario
/*
app.post(URL_BASE + 'users',function(req,res){
    totalUsers = userFile.length;

    let newUser = {
      userID : totalUsers+1,
      first_name: req.body.first_name,
      last_name : req.body.last_name,
      email : req.body.email,
      password : req.body.password
    }

    userFile.push(newUser);
    writeUserDataToFile(userFile,"./user.json");
    res.status(201);

    res.send({
              "mensaje" : "Usuario creado con exito.",
              "usuario":newUser
            });
})
*/


// POST 'users' mLab
app.post(URL_BASE + 'users',
 function(req, res){
   var clienteMlab = requestJSON.createClient(baseMLabURL);
   console.log(req.body);
   clienteMlab.get('user?' + apiKey,
     function(error, respuestaMLab, body){
       newID = body.length + 1;
       console.log("newID:" + newID);
       var newUser = {
         "id" : newID,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "password" : req.body.password
       };
       clienteMlab.post(baseMLabURL + "user?" + apiKey, newUser,
         function(error, respuestaMLab, body){
           console.log(body);
           res.status(201);
           res.send(body);
         });
     });
 });



//Logout OLD
app.post(URL_BASE + 'logout/:id',function(req,res){
  var mensaje = "";
  var status = null;
  for(user in userFile){
    if(req.body.userID == userFile[user].userID){
      console.log(userFile[user].logged);
      if(userFile[user].logged != undefined && userFile[user].logged != false){
          delete userFile[user].logged;
          console.log(userFile[user]);
          mensaje = 'Logout exitoso!';
          userLogouted = {
            userID:userFile[user].userID
          }
          writeUserDataToFile(userFile,"./user.json");
            status = 200;
          console.log(mensaje);
      }else {
        mensaje = 'El usuario no esta en session';
        status = 204;
      }
      break;
    }
  }

  if(status == null){
    mensaje = "No se econtraron coincidencias de IdUser."
    status = 204;
  }

  res.send({
    "mensaje": mensaje,
    "userLogouted":(status == 200 ? userLogouted:undefined)
  })
})

//Login OLD
/*
app.post(URL_BASE + 'login',function(req,res){
    var mensaje = "";
    var status = null;
    console.log('Usuario param:'+req.body.email);
    for(user in userFile){
      //console.log(userFile.hasOwnProperty(user));
      if(req.body.email == userFile[user].email){
        console.log('usuario enviado es igual a usuario registrado.');
        if(req.body.password == userFile[user].password){
          mensaje = "Logeo exitoso!"
          console.log(mensaje);
          userFile[user].logged = true;
          status = 200;
          userLoged = {
            email:userFile[user].email,
            password:userFile[user].password,
            userID:userFile[user].userID
          }
          writeUserDataToFile(userFile,"./user.json");
          break;
        }else{
          mensaje = "El password del usuario ingresado no coincide con el registrado.";
          console.log(mensaje);
          status = 500;
          break;
        }
      }
    }

    if(status == null){
      mensaje = "El usuario no se encuentra registrado.";
      status = 204;
    }

    res.send({
              "mensaje" : mensaje,
              "usuario":(status == 200 ?  userLoged: undefined)
            });
})
*/

//Method POST login
app.post(URL_BASE + "login",
 function (req, res){
   // console.log("POST /colapi/v3/login");
   let email = req.body.email;
   let pass = req.body.password;
   let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
   let limFilter = 'l=1&';
   let clienteMlab = requestJSON.createClient(baseMLabURL);
   clienteMlab.get('user?'+ queryString + limFilter + apiKey,
     function(error, respuestaMLab, body) {
       if(!error) {
         if (body.length == 1) { // Existe un usuario que cumple 'queryString'
           let login = '{"$set":{"logged":true}}';
           clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(login),
           //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
             function(errPut, resPut, bodyPut) {
               res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id});
               // If bodyPut.n == 1, put de mLab correcto
             });
         }
         else {
           res.status(404).send({"msg":"Usuario no válido."});
         }
       } else {
         res.status(500).send({"msg": "Error en petición a mLab."});
       }
   });
});


/**
Funcion para escribir un archivo.
**/

function writeUserDataToFile(data,path) {
   var jsonUserData = JSON.stringify(data);

   fs.writeFile(path, jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en "+path+".");
      }
    })
 }

app.listen(3000,function(){
    console.log('APITechU escuchando en el puerto 3000...')
});
